const express = require('express')
const morgan = require('morgan')
const { create } = require('express-handlebars')
const route = require('./route')
const db = require('./connect/db')
const app = express()
const port = 3000
const hbs = create({
    extname: '.hbs',
    helpers: {
        foo() {
            return 'FOO!'
        },
        bar() {
            return 'BAR!'
        },
    },
})

db()

app.use(express.static(`${__dirname}/public`))
app.use(morgan('combined'))

app.engine('.hbs', hbs.engine)
app.set('view engine', '.hbs')
app.set('views', 'src/resource/views')

route(app)

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
