const courses = require('./courses')

module.exports = (app) => {
    app.use('/courses', courses)

    app.use('/', (req, res, next) => {
        res.render('home')
    })
}
