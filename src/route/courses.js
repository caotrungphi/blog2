const express = require('express')
const router = express.Router()
const course = require('../controller/CourseController')

router.get('/', course.course)

module.exports = router
